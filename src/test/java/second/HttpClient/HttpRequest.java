package second.HttpClient;


import okhttp3.*;
import okhttp3.OkHttpClient.Builder;

import java.io.IOException;

public class HttpRequest {
    private Request request;
    private OkHttpClient client;

    public HttpRequest() {
        this.client = (new Builder()).build();
        this.request = (new okhttp3.Request.Builder()).url("http://something.ru").build();
    }

    public HttpRequest withHeader(String name, String value) {
        this.request = this.request.newBuilder().addHeader(name, value).build();
        return this;
    }

    public HttpRequest postContent(String url, String mediaTypeText, String content) {
        MediaType mediaType = MediaType.parse(mediaTypeText);
        RequestBody body = RequestBody.create(mediaType, content);
        this.request = this.request.newBuilder().post(body).url(url).build();
        return this;
    }

    public HttpRequest getContent(String url) {
        this.request = this.request.newBuilder().get().url(url).build();
        return this;
    }

    public int getStatusCode() {
        try{
            Response response = this.client.newCall(this.request).execute();
            Throwable var2 = null;
            int var3;
            try {
                var3 = response.code();
            } catch (Throwable var13) {
                var2 = var13;
                throw var13;
            } finally {
                if (response != null) {
                    if (var2 != null) {
                        try {
                            response.close();
                        } catch (Throwable var12) {
                            var2.addSuppressed(var12);
                        }
                    } else {
                        response.close();
                    }
                }
            }
            return var3;
        } catch (IOException var15) {
           var15.printStackTrace();
            return 0;
        }
    }

    public String send() {
        try {
            Response response = this.client.newCall(this.request).execute();
            Throwable var2 = null;
            String var3;
            try {
                var3 = response.body().string();
            } catch (Throwable var13) {
                var2 = var13;
                throw var13;
            } finally {
                if (response != null) {
                    if (var2 != null) {
                        try {
                            response.close();
                        } catch (Throwable var12) {
                            var2.addSuppressed(var12);
                        }
                    } else {
                        response.close();
                    }
                }
            }
            return var3;
        } catch (IOException var15) {
            return var15.getMessage();
        }
    }
}
