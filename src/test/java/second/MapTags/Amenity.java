package second.MapTags;

public class Amenity implements MapTags {

    private String value;

    public Amenity(String tagValue){
        value = tagValue;
    }

    @Override
    public String getTag() {
        return "[\"amenity\"=\""+value+"\"]";
    }
}
