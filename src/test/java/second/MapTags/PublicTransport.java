package second.MapTags;

public class PublicTransport implements MapTags {

    private String value;

    public  PublicTransport(String tagValue){
        value = tagValue;
    }
    @Override
    public String getTag() {
        return "[\"public_transport\"=\""+value+"\"]";
    }
}
