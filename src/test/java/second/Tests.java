package second;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import second.MapTags.Amenity;
import second.MapTags.PublicTransport;

public class Tests {

    private static ApiRequest request;

    @BeforeClass
    public static void setUp(){
        request = new ApiRequest().pickServer("http://overpass.openstreetmap.fr/api/interpreter");
    }

    @Test
    public void countBars() {
        String bars = request.buildCountRequest(400, new Amenity("bar"))
                .sendRequest();
        Assert.assertTrue("no bars in 400 meters", count(bars) != 0);
    }

    @Test
    public void countCafe() {
        String cafe = request.buildCountRequest(400, new Amenity("cafe"))
                .sendRequest();
        Assert.assertTrue("less then 2 cafe in range", count(cafe) > 1);
    }

    @Test
    public void countTransport() {
        String platform = request.buildCountRequest(400, new PublicTransport("platform"))
                .sendRequest();
        Assert.assertTrue("no platforms in 400 meters", count(platform) != 0);
    }

    private int count(String response) {
        return Integer.parseInt(response.split("\"nodes\": \"")[1].split("\",")[0]);
    }
}
