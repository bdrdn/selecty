package second;

import second.HttpClient.HttpRequest;
import second.MapTags.MapTags;

import java.net.URL;

class ApiRequest {

    private final String RAMBLER_URL = "http://overpass.osm.rambler.ru/cgi/interpreter";
    private final String DE_URL = "http://overpass-api.de/api/interpreter";

    private String url;
    private final String MEDIA_TYPE = "Content-Type :application/x-www-form-urlencoded; charset=UTF-8";
    private String request;

/*
поскольку без прокси  не простукивался ни один из данных в тестовом задании серверов реализован конструктор в качестве
аргумента принимающий адрес сервера, на который будет отправлен запрос в случае недоступности серверов из задания.
Так же, если подобная реализация неприемлема, ниже представлен конструктор без параметров.
Для проверки доступности сервера достаточно получить statusCode == 400 при пустом GET запросе. Сервер нашел ошибку в
синтаксисе, следовательно сервер работает. А поскольку GET на /interpreter не предусмотрен...
 */
    ApiRequest pickServer(String defaultUrl){
        System.out.println("Try to pick server");
        if (statusCode(RAMBLER_URL)==400) {
            System.out.println("Rambler server "+RAMBLER_URL);
            this.url = RAMBLER_URL;
        } else if (statusCode(DE_URL)==400){
            System.out.println("DE server "+DE_URL);
            this.url = DE_URL;
        } else if(statusCode(defaultUrl)==400){
            System.out.println("Urls from testTask doesn't work so... "+defaultUrl);
            this.url = defaultUrl;
        } else {
            throw new NullPointerException("All servers doesn't work");
        }
        return this;
    }

    ApiRequest pickServer(){
        System.out.println("Try to pick server");
        if (statusCode(RAMBLER_URL)!=0) {
            System.out.println("Rambler server "+RAMBLER_URL);
            this.url = RAMBLER_URL;
        } else if (statusCode(DE_URL)!=0){
            System.out.println("DE server "+DE_URL);
            this.url = DE_URL;
        } else {
            throw new NullPointerException("All servers doesn't work");
        }
        return this;
    }

    ApiRequest buildCountRequest(int distanceMeters, MapTags tags){
        System.out.println("try to build request with distance "+distanceMeters+" and tag "+tags.getTag());
        this.request = "[out:json][timeout:25];(node(around:"+distanceMeters+", 59.93823555, 30.2668659740719)" +
                tags.getTag()+");out count;";
        return this;
    }

    String sendRequest(){
        return new HttpRequest().postContent(url,MEDIA_TYPE,request).send();
    }

    private int statusCode(String serverUrl){
        return new HttpRequest().getContent(serverUrl).getStatusCode();
    }

}
