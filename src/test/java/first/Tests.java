package first;

import first.base.Vertex;
import org.junit.Assert;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;

public class Tests {

    @Test
    public void triangleFrom3Vertex() {
        first.base.Polygon triangle = new TriangleFactory(new Vertex(1, 5), new Vertex(3, 4), new Vertex(5, 6))
                .getTriangle();
        Assert.assertFalse("Triangle is line", isLine(triangle));
    }

    @Test
    public void triangleFromVertexArray() {
        Vertex[] verticesArray = new Vertex[]{new Vertex(1, 2), new Vertex(2, 4), new Vertex(4, 6)};
        first.base.Polygon triangle = new TriangleFactory(verticesArray).getTriangle();
        Assert.assertFalse("Triangle is line", isLine(triangle));
    }

    @Test(expected = IllegalArgumentException.class)
    public void wrongArrayLength() {
        Vertex[] wrongLength = new Vertex[]{new Vertex(0, 0)};
        new TriangleFactory(wrongLength).getTriangle();
        Assert.fail("triangle from not 3 vertices");
    }

    @Test(expected = IllegalArgumentException.class)
    public void fromArrayOfNulls() {
        Vertex[] nulls = new Vertex[]{null, null, null};
        new TriangleFactory(nulls).getTriangle();
        Assert.fail("triangle from Array of nulls");
    }

    @Test(expected = IllegalArgumentException.class)
    public void fromNullArgs(){
        new TriangleFactory(null,null,new Vertex(2,3));
        Assert.fail("Triangle from null");
    }

    private boolean isLine(first.base.Polygon triangle) {
        //if (x1-x3)/(x2-x3)==(y1-y3)/(y2-y3) isLine
        System.out.println("Check for line");
        List<Integer> x = new ArrayList<>();
        List<Integer> y = new ArrayList<>();
        for (Vertex vertex : triangle.vertices) {
            x.add(vertex.x);
            y.add(vertex.y);
        }
        int left = (x.get(0) - x.get(2)) / (x.get(1) - x.get(2));
        int right = (y.get(0) - y.get(2)) / (y.get(1) - y.get(2));
        return left == right;
    }
}