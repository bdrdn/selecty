package first;

import first.base.Polygon;
import first.base.Vertex;

import java.util.Arrays;

public class TriangleFactory implements first.base.TriangleFactory {
    private Vertex[] edges;
/*
поскольку нет никаких комментариев о реализации сделал простую, без проверок. Логгирования в требованиях не было, потому
вывод только через println.
 */
    TriangleFactory(Vertex vertexA, Vertex vertexB, Vertex vertexC) {
        System.out.println("Try to create triangle from 3 Vertex");
        this.edges = new Vertex[]{
                notNull(vertexA),
                notNull(vertexB),
                notNull(vertexC)};
    }

    TriangleFactory(Vertex[] vertices) {
        System.out.println("Try to create triangle from Vertex[]");
        if (Arrays.asList(vertices).contains(null)) throw new IllegalArgumentException("Vertex[] can't be null");
        if (vertices.length == 3) {
            this.edges = vertices;
        } else {
            throw new IllegalArgumentException("Triangle must have 3 vertices");
        }
    }

    public Polygon getTriangle() {
        return new Polygon(edges);
    }

    private Vertex notNull(Vertex vertex) {
        if (vertex == null) throw new IllegalArgumentException("vertex can't be null");
        return vertex;
    }
}
